package com.esp.project.wasteclassifier.mainactivity;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.esp.project.wasteclassifier.R;
import com.esp.project.wasteclassifier.cameraactivity.SaveActivity;

import java.util.List;

import database.WasteBaseHelper;


/**
 * This class provides the implementation of the hystory activity and its functionalities.
 */
public class HistoryActivity extends AppCompatActivity {

    private static final String EDIT_TEXT_KEY = "Edit user item";
    private final String EDIT_STR = "Modifica";
    private final String EMPTY_STR_EDITTEXT = "Questo campo non può essere vuoto";

    private int position;
    private boolean savingMode;
    private String swipedWasteName;
    private String containerName;
    private EditText editText;
    private RecyclerView mRecyclerView;
    private AlertDialog.Builder alertDialog;
    private UserWasteListAdapter mAdapter;
    private WasteBaseHelper wasteBaseHelper;
    private List<String> userWasteList;
    private final Paint p = new Paint();

    /**
     * Called when the activity is starting.
     * @param savedInstanceState the saved state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = findViewById(R.id.my_toolbar);

        //Get intent from MainActivity
        final Intent intent = getIntent();

        //Get taken image class
        containerName = intent.getStringExtra(MainActivity.CONTAINER_TO_HISTORYACTIVITY);

        toolbar.setTitle(containerName);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            // Enable the Up button
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // open the database and retrieve the wastes for the specific container
        wasteBaseHelper = new WasteBaseHelper(this);
        userWasteList = wasteBaseHelper.findUserWaste(containerName);

        // Get a handle to the RecyclerView.
        mRecyclerView = findViewById(R.id.recycleViewUserWaste);
        // Create an adapter and supply the data to be displayed.
        mAdapter = new UserWasteListAdapter(this, userWasteList);
        // Connect the adapter with the RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
        // Give the RecyclerView a default layout manager.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // swipe to delete and edit using the touch helper
        ItemTouchCallback itemTouchCallback = new ItemTouchCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);

        // attaching the touch helper to recycler view
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    /**
     * Creates the menu in the toolbar
     * @param menu the reference to the Menu object
     * @return true for the menu to be displayed; false it will not be shown.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the activity_main_menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_history_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Manage the items in the toolbar
     * @param item the reference of the item
     * @return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.settings_item_history_activity) {
            int listSize = userWasteList.size();
            WasteBaseHelper wasteBaseHelper = new WasteBaseHelper(this);
            wasteBaseHelper.deleteAllUserWastes(containerName);
            userWasteList.clear();
            mAdapter.notifyItemRangeRemoved(0, listSize);
            Toast.makeText(HistoryActivity.this, "Tutti i rifiuti eliminati", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Callback to manage the swipe and the background of an item of the RecycleView
     */
    private class ItemTouchCallback extends ItemTouchHelper.SimpleCallback
    {
        /**
         * Instantiates a new Item touch callback.
         *
         * @param dragDirs  the drag dirs
         * @param swipeDirs the swipe dirs
         */
        ItemTouchCallback(int dragDirs, int swipeDirs) {
            super(dragDirs, swipeDirs);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
            return false;
        }

        /**
         * Called when a ViewHolder is swiped by the user. If it is a left swipe edit the item, otherwise delete it
         * @param viewHolder The ViewHolder which has been swiped by the user.
         * @param direction The direction to which the ViewHolder is swiped.
         */
        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

            position = viewHolder.getAdapterPosition();
            swipedWasteName = mAdapter.getUserWasteAt(position);

            // manage of the possible swipes
            if(direction == ItemTouchHelper.LEFT)   // remove the item
            {
                wasteBaseHelper.deleteUserWaste(swipedWasteName, containerName);
                mAdapter.removeUserItem(position);
                Toast.makeText(HistoryActivity.this, "Rifiuto eliminato", Toast.LENGTH_SHORT).show();
            }
            else    // edit the item
            {
                alertDialog = new AlertDialog.Builder(HistoryActivity.this, R.style.AlertDialogTheme);
                View view = View.inflate(HistoryActivity.this, R.layout.edit_alert_dialog, null);
                editText = view.findViewById(R.id.edittext_waste);

                // check if the text written by the user is non empty and less than 50 character
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    /**
                     * This method is called to notify the user if the name of the waste is empty
                     * and when the he reaches the maximum number of characters allowed.
                     * @param s the sequence of characters
                     * @param i the starting point
                     * @param i1 the length of the old text
                     * @param i2 the number of characters changed
                     */
                    @Override
                    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                        if(s.length() == 0){
                            Toast t = Toast.makeText(HistoryActivity.this, R.string.minLength_warning, Toast.LENGTH_SHORT);
                            t.show();
                        }

                        if(s.length() == SaveActivity.MAX_LENGTH) {
                            Toast t = Toast.makeText(HistoryActivity.this, R.string.maxLength_warning, Toast.LENGTH_SHORT);
                            t.show();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                alertDialog.setView(view);

                // set a positive button and when clicked update the database and the list of the activity
                alertDialog.setPositiveButton("Salva", new DialogInterface.OnClickListener() {

                    /**
                     * Called when the save button of the alertDialog is clicked. It eventually update
                     * the record in the database and the list of the RecycleView
                     * @param dialog the dialog that received the click
                     * @param which the button that was clicked or the position of the item clicked
                     */
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String newWasteName  = editText.getText().toString();

                        if (newWasteName.isEmpty())
                        {
                            mAdapter.notifyItemChanged(userWasteList.indexOf(swipedWasteName));
                        }
                        else
                        {
                            newWasteName = newWasteName.substring(0, 1).toUpperCase() + newWasteName.substring(1).toLowerCase();
                            if (mAdapter.updateUserItem(swipedWasteName, newWasteName) == 0) {
                                wasteBaseHelper.updateUserWaste(swipedWasteName, newWasteName, containerName);
                                Toast.makeText(HistoryActivity.this, "Rifiuto aggiornato", Toast.LENGTH_SHORT).show();
                            } else
                                Toast.makeText(HistoryActivity.this, "Rifiuto già presente", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                });

                // manage the back button when the alert is visible
                alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    /**
                     * Called when the back button is clicked while the alertDialog is visible.
                     * @param dialog the dialog that received the click
                     */
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        mAdapter.notifyItemChanged(position);
                        dialog.dismiss();
                    }
                });

                alertDialog.setTitle(EDIT_STR);
                editText.setText(swipedWasteName);
                alertDialog.show();
            }
        }

        /**
         * Called by ItemTouchHelper on RecyclerView's onDraw callback. It draw the background of a ViewHolder
         * when it is swiped
         * @param c The canvas which RecyclerView is drawing its children
         * @param recyclerView The RecyclerView to which ItemTouchHelper is attached to
         * @param viewHolder The ViewHolder which is being interacted by the User or it was interacted and simply animating to its original position
         * @param dX The amount of horizontal displacement caused by user's action
         * @param dY The amount of vertical displacement caused by user's action
         * @param actionState The type of interaction on the View.
         * @param isCurrentlyActive True if this view is currently being controlled by the user or false it is simply animating back to its original state.
         */
        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

            // manage the background of the screen when an item is swiped
            // blue background when edit, red background when deleting
            Bitmap icon;
            if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                View itemView = viewHolder.itemView;
                float height = (float) itemView.getBottom() - (float) itemView.getTop();
                float width = height / 3;
                RectF icon_dest;

                if(dX > 0){     // right swipe -> edit
                    p.setColor(getResources().getColor(R.color.light_blue));
                    RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                    c.drawRect(background,p);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_edit_white);
                    icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                } else {        // left swipe -> delete
                    p.setColor(getResources().getColor(R.color.red));
                    RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                    c.drawRect(background,p);
                    icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white);
                    icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                }
                c.drawBitmap(icon, null, icon_dest, p);
            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

        }
    }
}

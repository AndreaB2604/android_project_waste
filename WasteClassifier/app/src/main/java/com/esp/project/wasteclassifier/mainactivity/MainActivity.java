package com.esp.project.wasteclassifier.mainactivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.esp.project.wasteclassifier.OnBoardingActivity;
import com.esp.project.wasteclassifier.R;
import com.esp.project.wasteclassifier.SettingsActivity;
import com.esp.project.wasteclassifier.cameraactivity.CameraActivity;
import com.esp.project.wasteclassifier.searchactivity.SearchActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import database.WasteBaseHelper;
import lecho.lib.hellocharts.listener.PieChartOnValueSelectListener;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

/**
 * This class provides the implementation of the main activity and its functionalities.
 */
public class MainActivity extends AppCompatActivity {
    
    private Toolbar toolbar;
    private TextView mMainText, mNoChartText;
    private FloatingActionButton fab;
    private PieChartView pieChartView;
    private final long DAY_MILLISECONDS = 1000L * 60 * 60 * 24;
    private final int[] COLORS = {R.color.teal, R.color.green, R.color.light_green, R.color.amber, R.color.orange,R.color.brown};

    /**
     * The constant CONTAINER_TO_HISTORYACTIVITY to save information for the Intent of the HistoryActivity.
     */
    public static final String CONTAINER_TO_HISTORYACTIVITY = "The container selected by the user";

    private SharedPreferences prefs = null;

    /**
     * Called when the activity is starting.
     * @param savedInstanceState the saved state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // this call creates the database on first run,
        // if the database has been already created it does nothing
        (new WasteBaseHelper(this)).getReadableDatabase();

        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        mMainText = findViewById(R.id.mainText);
        mNoChartText = findViewById(R.id.noChart);

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CameraActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Creates the menu in the toolbar
     * @param menu the reference to the Menu object
     * @return true for the menu to be displayed; false it will not be shown.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the activity_main_menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return true;
    }

    /**
     * Called whenever an item in your options menu is selected and managed the actions to perform
     * @param item The menu item that was selected.
     * @return Return false to allow normal menu processing to proceed, true to consume it here.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
    
        if (id == R.id.search_item) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.settings_item) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Called to redraw every time the pie chart.
     */
    @Override
    protected void onResume() {
        if ((!prefs.contains("firstrun") || (prefs.contains("firstrun") && prefs.getBoolean("firstrun", true)))) {
            // FIRST RUN ONBOARDING ACTIVITY
            Log.e("MAIN", "TRYING TO ACCESS TO ONBOARDING");
            Intent intent = new Intent(this, OnBoardingActivity.class);
            startActivity(intent);
        }

        WasteBaseHelper wasteBaseHelper = new WasteBaseHelper(this);
        pieChartView = findViewById(R.id.chart);

        // Get the period value from the SharedPreferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int days = Integer.parseInt(prefs.getString("statsPeriod", "30"));

        int i = 0;
        List<SliceValue> pieData = new ArrayList<>();
        Map<String, Long> lastContainerMap = wasteBaseHelper.getLastContainers(DAY_MILLISECONDS * days);

        if(!lastContainerMap.isEmpty())
        {
            pieChartView.setVisibility(View.VISIBLE);
            setTextViews(true, days);
            // Get the total of scans to compute percentages
            long tot = 0;
            for(Map.Entry<String, Long> entry : lastContainerMap.entrySet()) tot += entry.getValue();

            for(Map.Entry<String, Long> entry : lastContainerMap.entrySet()) {
                double percentage = (entry.getValue() * 100.0) / tot;
                SliceValue sliceValue = new SliceValue(entry.getValue());
                sliceValue.setColor(getResources().getColor(COLORS[i++]));
                sliceValue.setLabel(entry.getKey() + " " + String.format(Locale.ITALIAN,"%.1f", percentage) + "%");
                pieData.add(sliceValue);
            }
            // Here we will read from the database the scans of the last 30-days.
            // We will then compute a simple percentage of each waste class
            // And finally we will graph them thanks to this library

            // This is used to include data inside our chart
            PieChartData pieChartData = new PieChartData(pieData);
            pieChartData.setHasLabels(true);
            // Therefore we attach this data to the final displayed chart
            pieChartView.setPieChartData(pieChartData);
            pieChartView.setOnValueTouchListener(new PieChartOnValueSelectListener(){
                @Override
                public void onValueSelected(int index, SliceValue slice){
                    String string = new String(slice.getLabelAsChars());
                    String containerName = string.split(" ")[0];
                    Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
                    intent.putExtra(CONTAINER_TO_HISTORYACTIVITY, containerName);
                    // Close the tflite thread and finish the activity
                    startActivity(intent);
                }

                public void onValueDeselected() {}
            });
        }
        else {
            pieChartView.setVisibility(View.INVISIBLE);
            setTextViews(false, 0);
        }

        super.onResume();
    }

    /**
     * Show the welcome text in the MainActivity based on the period of time selected for the statistics
     * @param isChart true if the pie chart is visible, false otherwise
     * @param days the number of days of the statistics to show
     */
    private void setTextViews(boolean isChart, int days) {
        if(!isChart) {
            mMainText.setText("");
            mNoChartText.setText(R.string.noPieChart);
        }
        else {
            String s = "";
            switch(days) {
                case 7:
                    s = getString(R.string.week);
                    break;
                case 30:
                    s = getString(R.string.month);
                    break;
                case 91:
                    s = getString(R.string.threemonths);
                    break;
                case 182:
                    s = getString(R.string.sixmonths);
                    break;
                case 365:
                    s = getString(R.string.year);
                    break;
                case 0:
                    s = getString(R.string.ever);
                    break;
            }
            String welcome = getString(R.string.welcomeText);
            String mainPeriodText = getString(R.string.mainPeriodText);
            mMainText.setText(welcome+"\n"+mainPeriodText+" "+s);
            mNoChartText.setText("");
        }
    }
}





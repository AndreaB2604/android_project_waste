package com.esp.project.wasteclassifier.searchactivity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esp.project.wasteclassifier.R;

import java.util.ArrayList;
import java.util.List;

import database.Waste;

/**
 * This class provides the Adapter for the search activity.
 * Adapters provide a binding from an app-specific data set to views
 * that are displayed within a RecyclerView.
 */
public class WasteListAdapter extends RecyclerView.Adapter<WasteListAdapter.WasteViewHolder>  {
	
	private final List<Waste> mWasteList;
	private final LayoutInflater mInflater;

    /**
     * Creates a new WasteListAdapter with the given context and the given list
     *
     * @param context   the context of the activity
     * @param wasteList the list of wastes
     */
    public WasteListAdapter(Context context, List<Waste> wasteList) {
		mInflater = LayoutInflater.from(context);
		mWasteList = (wasteList != null)? wasteList : new ArrayList<Waste>();
	}

	/**
	 * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item
	 * @param viewGroup the ViewGroup into which the new View will be added after it is bound to an adapter position
	 * @param i the view type of the new View.
	 * @return 	A new WasteViewHolder that holds a View.
	 */
	@NonNull
	@Override
	public WasteListAdapter.WasteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View mItemView = mInflater.inflate(R.layout.searchactivity_wastelist_item, viewGroup, false);
		return new WasteViewHolder(mItemView, this);
	}

	/**
	 * Called by RecyclerView to display the data at the specified position.
	 * @param wasteViewHolder the WasteViewHolder which should be updated to represent the contents
	 *                        of the item at the given position in the data set.
	 * @param i the position of the item within the adapter's data set.
	 */
	@Override
	public void onBindViewHolder(@NonNull WasteListAdapter.WasteViewHolder wasteViewHolder, int i) {
		String mWasteName = mWasteList.get(i).getName();
		String mContainerName = mWasteList.get(i).getContainer();
		wasteViewHolder.wasteItemView.setText(mWasteName);
		wasteViewHolder.containerItemView.setText(mContainerName);
	}

	/**
	 * @return the number of the item in the list
	 */
	@Override
	public int getItemCount() {
		return mWasteList.size();
	}

    /**
     * An element of the WasteListAdapter
     */
    class WasteViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Waste item view.
         */
        final TextView wasteItemView;
        /**
         * The Container item view.
         */
        final TextView containerItemView;
        /**
         * The M adapter.
         */
        final WasteListAdapter mAdapter;

        /**
         * Creates a new WasteViewHolder that can be used in an Adapter
         *
         * @param itemView the reference to the view
         * @param adapter  the reference to the adapter
         */
        WasteViewHolder(View itemView, WasteListAdapter adapter) {
			super(itemView);
			wasteItemView = itemView.findViewById(R.id.waste_name);
			containerItemView = itemView.findViewById(R.id.waste_container);
			mAdapter = adapter;
		}
	}
}

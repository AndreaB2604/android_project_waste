package com.esp.project.wasteclassifier;

import android.animation.ArgbEvaluator;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.esp.project.wasteclassifier.cameraactivity.CameraActivity;

/**
 * This class provides the implementation of the on boarding activity on the first run of the application.
 */
public class OnBoardingActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private ImageButton mNextBtn;
    private Button mSkipBtn, mFinishBtn;

    private ImageView zero, one, two;
    private ImageView[] indicators;

    private CoordinatorLayout mCoordinator;
    private static final String TAG = "OnBoardingActivity";

    private int page = 0;

    /**
     * Called when the activity is starting.
     * @param savedInstanceState the saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("TAG", "onCreate()");

        setContentView(R.layout.onboarding_activity);
        setSharedPreferences();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mNextBtn = findViewById(R.id.intro_btn_next);

        mSkipBtn = findViewById(R.id.intro_btn_skip);
        mFinishBtn = findViewById(R.id.intro_btn_finish);

        zero = findViewById(R.id.intro_indicator_0);
        one  = findViewById(R.id.intro_indicator_1);
        two  = findViewById(R.id.intro_indicator_2);

        mCoordinator = findViewById(R.id.main_content);

        indicators = new ImageView[]{zero, one, two};

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setCurrentItem(page);
        updateIndicators(page);

        // I create the three colors that will fill my three fragment backgrounds
        final int color1 = ContextCompat.getColor(this, R.color.light_green);
        final int color2 = ContextCompat.getColor(this, R.color.red);
        final int color3 = ContextCompat.getColor(this, R.color.light_blue);
        final int[] colorList = new int[]{color1, color2, color3};

        final ArgbEvaluator evaluator = new ArgbEvaluator();
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // Color update when i scroll / click next
                // evaluator helps us evaluating which color to set depending on
                // which EXACT positionOffset we are in, this means that it will change the color
                // At half the way between a fragment and another one, instead of changing it just one you
                // completely scroll it
                int colorUpdate = (Integer) evaluator.evaluate(positionOffset, colorList[position], colorList[position == 2 ? position : position + 1]);
                mViewPager.setBackgroundColor(colorUpdate);
            }

            @Override
            public void onPageSelected(int position) {
                page = position;
                updateIndicators(page);

                switch (position) {
                    case 0:
                        mViewPager.setBackgroundColor(color1);
                        break;
                    case 1:
                        mViewPager.setBackgroundColor(color2);
                        askPermissions();
                        break;
                    case 2:
                        mViewPager.setBackgroundColor(color3);
                        break;
                }
                mNextBtn.setVisibility(position == 2 ? View.GONE : View.VISIBLE);
                mFinishBtn.setVisibility(position == 2 ? View.VISIBLE : View.GONE);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                // has to be overridden
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page += 1;
                mViewPager.setCurrentItem(page, true);
            }
        });

        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putBoolean("firstrun", false).apply();
                finish();
            }
        });

        mFinishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putBoolean("firstrun", false).apply();
                finish();
            }
        });

    }

    /**
     * Ask the permissions for the camera usage and the memory storage.
     */
    private void askPermissions() {
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        if (!CameraActivity.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            Log.d(TAG, "Permission asked");
        }
    }

    /**
     * Set the default preferences of the application
     */
    private void setSharedPreferences() {
        Log.d("ONBOARDING: ", "FIRST RUN");
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.edit().putString("statsPeriod", "30").apply();
        sharedPreferences.edit().putBoolean("darkTheme", false).apply();
        sharedPreferences.edit().putBoolean("sendResults", false).apply();
        sharedPreferences.edit().putString("languageType", "1").apply();
    }

    private void updateIndicators(int position) {
        // set the selected indicator icon corresponding to the current page position
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }

    /**
     * The type Placeholder fragment.
     */
    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * The Img.
         */
        ImageView img;

        /**
         * The Bgs.
         */
        final int[] bgs = new int[]{R.drawable.waste, R.drawable.permissions, R.drawable.start};

        /**
         * Instantiates a new Placeholder fragment.
         */
        public PlaceholderFragment() {
        }

        /**
         * New instance placeholder fragment.
         *
         * @param sectionNumber the section number
         * @return the placeholder fragment
         */
        static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        /**
         * Called to have the fragment instantiate its user interface view.
         * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment
         * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
         * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state as given here.
         * @return Return the View for the fragment's UI, or null.
         */
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_pager, container, false);
            Log.e(TAG, ""+getArguments().getInt(ARG_SECTION_NUMBER));
            TextView pagelabel = rootView.findViewById(R.id.section_label);
            TextView pagedescription = rootView.findViewById(R.id.pagedescription);
            String s_label = "";
            String s_desc = "";
            switch(getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1:
                    s_label = getResources().getString(R.string.app_name);
                    s_desc = getResources().getString(R.string.desc1);
                    break;

                case 2:
                    s_label = getResources().getString(R.string.askpermissions);
                    s_desc = getResources().getString(R.string.desc2);
                    break;
                case 3:
                    s_label = getResources().getString(R.string.startapp);
                    s_desc = getResources().getString(R.string.desc3);
                    break;
            }
            pagelabel.setText(s_label);
            pagedescription.setText(s_desc);

            img = rootView.findViewById(R.id.section_img);
            img.setBackgroundResource(bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);


            return rootView;
        }
    }

    /**
     * The type Sections pager adapter.
     */
    class SectionsPagerAdapter extends FragmentPagerAdapter {
        /**
         * Instantiates a new Sections pager adapter.
         *
         * @param fm the fragment manager
         */
        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * @param position the position of the fragment to return.
         * @return the new instance of the Fragment
         */
        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);

        }

        /**
         * @return the number of total fragment
         */
        @Override
        public int getCount() {
            return 3;
        }
    }
}

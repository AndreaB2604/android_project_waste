package com.esp.project.wasteclassifier.mainactivity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.esp.project.wasteclassifier.R;

import java.util.ArrayList;
import java.util.List;

/**
 * This class provides the Adapter for the history activity.
 * Adapters provide a binding from an app-specific data set to views
 * that are displayed within a RecyclerView.
 */
public class UserWasteListAdapter extends RecyclerView.Adapter<UserWasteListAdapter.UserWasteViewHolder> {

    private final List<String> mUserWasteList;
    private final LayoutInflater mInflater;

    /**
     * Creates a new UserWasteListAdapter with the given context and the given list
     *
     * @param context           the context of the activity
     * @param userWasteListList the list of wastes
     */
    public UserWasteListAdapter(Context context, List<String> userWasteListList) {
        mInflater = LayoutInflater.from(context);
        mUserWasteList = (userWasteListList != null)? userWasteListList : new ArrayList<String>();
    }

    /**
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item
     * @param viewGroup the ViewGroup into which the new View will be added after it is bound to an adapter position
     * @param i the view type of the new View.
     * @return A new WasteViewHolder that holds a View.
     */
    @NonNull
    @Override
    public UserWasteListAdapter.UserWasteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mItemView = mInflater.inflate(R.layout.historyactivity_wastelist_item, viewGroup, false);
        return new UserWasteViewHolder(mItemView, this);
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     * @param wasteViewHolder the WasteViewHolder which should be updated to represent the contents
     *                        of the item at the given position in the data set.
     * @param i the position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull UserWasteListAdapter.UserWasteViewHolder wasteViewHolder, int i) {
        String mWasteName = mUserWasteList.get(i);
        wasteViewHolder.userWasteItemView.setText(mWasteName);
    }

    /**
     * @return the number of the item in the list
     */
    @Override
    public int getItemCount() {
        return mUserWasteList.size();
    }

    /**
     * Return the name of the waste in the specific position
     *
     * @param position the position of the waste to return
     * @return the name of the waste in the specific position
     */
    public String getUserWasteAt(int position) {
        return mUserWasteList.get(position);
    }


    /**
     * An element of the UserWasteListAdapter
     */
    class UserWasteViewHolder extends RecyclerView.ViewHolder {
        /**
         * The User waste item view.
         */
        final TextView userWasteItemView;
        /**
         * The M adapter.
         */
        final UserWasteListAdapter mAdapter;

        /**
         * Creates a new UserWasteViewHolder that can be used in an Adapter
         *
         * @param itemView the reference to the view
         * @param adapter  the reference to the adapter
         */
        UserWasteViewHolder(View itemView, UserWasteListAdapter adapter) {
            super(itemView);
            userWasteItemView = itemView.findViewById(R.id.user_waste_name);
            mAdapter = adapter;
        }
    }

    /**
     * Remove the item in a specific position from the list and notify the deletion
     *
     * @param position the position of the item to remove
     */
    public void removeUserItem(int position)
    {
        mUserWasteList.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * Replace the oldItem with newItem.
     *
     * @param oldItem the name of the old item
     * @param newItem the name of the new item
     * @return 0 on successful update, 1 if the new item is already present
     */
    public int updateUserItem(String oldItem, String newItem)
    {
        int oldIndex = mUserWasteList.indexOf(oldItem);
        if(oldItem.equals(newItem) || mUserWasteList.contains(newItem))
        {
            notifyItemChanged(oldIndex);
            return 1;
        }
        int newIndex;
        for(newIndex = 0; newIndex < mUserWasteList.size() && mUserWasteList.get(newIndex).compareTo(newItem) < 0; newIndex++);
        int itemCount = Math.abs(newIndex - oldIndex) + 1;
        mUserWasteList.add(newIndex, newItem);
        mUserWasteList.remove(oldItem);

        notifyItemRangeChanged(Math.min(oldIndex, newIndex), itemCount);
        return 0;
    }
}

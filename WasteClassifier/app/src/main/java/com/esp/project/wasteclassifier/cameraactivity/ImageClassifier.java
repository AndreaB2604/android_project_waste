package com.esp.project.wasteclassifier.cameraactivity;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.esp.project.wasteclassifier.Classifier.SVC;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * This class provides the implementation of the classification for an image
 */
public class ImageClassifier extends AppCompatActivity {

    private static final int DIM_IMG_SIZE_X = 224;
    private static final int DIM_IMG_SIZE_Y = 224;
    private final int[] intValues = new int[DIM_IMG_SIZE_X * DIM_IMG_SIZE_Y];

    private static final int DIM_PIXEL_SIZE = 3;
    private static final int DIM_FLOAT = 4;
    private static final int IMAGE_MEAN = 0;
    private static final float IMAGE_STD = 255.0f;

    private static final int MODEL_INPUT_SIZE = DIM_PIXEL_SIZE * DIM_IMG_SIZE_Y * DIM_IMG_SIZE_X * DIM_FLOAT;

    private static final int RESULTS_TO_SHOW = 3;
    private static final String TAG = "ImageClassifier--->";

    private static final String MODEL_PATH = "mobilenet_model.tflite";

    private Interpreter tflite;

    private Bitmap imgBitmap = null;
    private ByteBuffer imgData = null;

    private float[][] feature_extracted;

    /**
     * Creation of classification class
     * @param savedInstanceState saved state
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Measuring time perfomance of classificator
        Log.d(TAG, "Begin of classification");
        long startTime = SystemClock.uptimeMillis();

        //Get intent from CameraActivity
        Intent intent = getIntent();

        //Get taken image path
        String Path = intent.getStringExtra("BitmapPath");


        try {

            //Bitmap prepared as dataset images (224x224)
            Bitmap bitmap = BitmapFactory.decodeFile(Path);
            imgBitmap = Bitmap.createScaledBitmap(
                    bitmap,
                    224,
                    224,
                    false
            );

        } catch (Exception error) {
            error.printStackTrace();
        }

        Log.d(TAG, "Image Classifier class started by Camera Intent; Classifying the taken image");

        try {
            tflite = new Interpreter(loadModelFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Created a Tensorflow Lite Image Classifier.");

        feature_extracted = new float[1][1280];

        //classification of the bitmap
        String estimation = classifyFrame(imgBitmap);

        //Put the result on the intent with the properly resultCode
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",estimation);
        setResult(RESULT_OK,returnIntent);

        // Close the tflite thread and finish the activity
        close();
        finish();

        //Calculate total time performance
        long endTime = SystemClock.uptimeMillis();
        Log.d(TAG, "Timecost to classification process: " + (endTime - startTime));
        Log.d(TAG, "END of classification");


    }
    
    /**
     * Classifies a frame from the preview stream
     * @param bitmap bitmap used for prediction
     * @return estimation of the bitmap (string of the result)
     */
    private String classifyFrame(Bitmap bitmap) {
        if (tflite == null) {
            Log.e(TAG, "Image classifier has not been initialized; Skipped.");
            return "Uninitialized Classifier.";
        }
        Log.d(TAG, "Converting bitmap into bytebuffer");
        convertBitmapToByteBuffer(bitmap);
        // Here's where the magic happens!!!
        long startTime = SystemClock.uptimeMillis();
        Log.d(TAG, "Performing feature extraction through MobileNet CNN model");
        tflite.run(imgData, feature_extracted);
        long endTime = SystemClock.uptimeMillis();
        Log.d(TAG, "Timecost to run model inference: " + (endTime - startTime));

        //Initialization of the classifier
        SVC clf = new SVC();
        //Make the prediction
        String estimation = clf.predict((feature_extracted[0]));

        Log.d(TAG, "CLASSIFIED AS: "+estimation);
        return estimation;

    }

    /**
     * Memory-map the model file in Assets.
     * @return the file mapped
     * @throws IOException if any error occurs
     */
    private MappedByteBuffer loadModelFile() throws IOException {
        AssetFileDescriptor fileDescriptor = getAssets().openFd(MODEL_PATH);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    /**
     * Writes Image data into a {@code ByteBuffer}.
     * @param bitmap bitmap used for prediction
     */
    private void convertBitmapToByteBuffer(Bitmap bitmap) {
        if (imgData == null) {
            imgData = ByteBuffer.allocateDirect(MODEL_INPUT_SIZE);
            imgData.order(ByteOrder.nativeOrder());
        }

        bitmap.getPixels(intValues, 0, 224, 0, 0, 224, 224);
        // Convert the image to floating point.
        int pixel = 0;
        long startTime = SystemClock.uptimeMillis();
        //normalization of the bitmap
        for (int i = 0; i < DIM_IMG_SIZE_X; ++i) {
            for (int j = 0; j < DIM_IMG_SIZE_Y; ++j) {
                final int val = intValues[pixel++];
                imgData.putFloat((( (val >> 0) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                imgData.putFloat((( (val >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                imgData.putFloat((( (val >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
            }
        }

        //Measure performance of ByteBuffer conversion
        long endTime = SystemClock.uptimeMillis();
        Log.d(TAG, "Timecost to put values into ByteBuffer: " + (endTime - startTime));

    }

    /**
     * Close the tflite thread
     */
    private void close() {
        tflite.close();
        tflite = null;
    }
}

package com.esp.project.wasteclassifier.cameraactivity;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * This class provides the implementation of the preview for the camera activity
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private final SurfaceHolder mHolder;
    private Camera mCamera;


    /**
     * Constructor camera preview
     *
     * @param context context associated
     * @param camera  camera on which build the preview
     */
    public CameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;
        //access and control over this SurfaceView's underlying surface
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    /**
     * Creation of CameraPreview and start the preview
     * @param holder holder of the surface
     */
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            // create the surface and start camera preview
            if (mCamera == null) {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            }
        } catch (IOException e) {
            Log.d(VIEW_LOG_TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    /**
     * Associate the surfaceHolder and start the preview
     *
     * @param camera camera object (permission needed)
     */
    public void refreshCamera(Camera camera) {
        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }
        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }
        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        setCamera(camera);
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (Exception e) {
            Log.d(VIEW_LOG_TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    /**
     * Handle changed of surface view as rotation: make sure to stop the preview before resizing or reformatting it
     * @param holder holder of the surface
     * @param format format of the surface
     * @param w width of surface
     * @param h height of the surface
     */
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

        refreshCamera(mCamera);
    }

    /**
     * Set camera object
     * @param camera camera object
     */
    private void setCamera(Camera camera) {
        //method to set a camera instance
        mCamera = camera;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {}

}